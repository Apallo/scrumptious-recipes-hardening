from django import template

register = template.Library()


# def resize_to(ingredient, target):
#     print("value:", value)
#     print("arg:", arg)
#     return "resize done"
def resize_to(ingredient, target):
    # Get the number of servings from the ingredient's
    # recipe using the ingredient.recipe.servings
    # properties
    servings = ingredient.recipe.servings
    # If the servings from the recipe is not None
    #   and the value of target is not None
    if servings is not None and target is not None:
        if int(target) < 0:
            return "Can't serve negative portions!"
        else:  # try
            # calculate the ratio of target over
            #   servings
            # return the ratio multiplied by the
            #   ingredient's amount
            # catch a possible error
            # pass
            try:
                ratio = int(target) / servings
                return int(ingredient.amount) * ratio
            except Exception:
                pass
    # return the original ingredient's amount since
    #   nothing else worked
    return ingredient


register.filter(resize_to)
