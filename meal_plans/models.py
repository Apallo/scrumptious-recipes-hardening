from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


class MealPlan(models.Model):
    name = models.CharField(max_length=250)
    date = models.DateField()
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="user_meal_plans",
        on_delete=models.CASCADE,
        null=True,
    )
    recipes = models.ManyToManyField(
        "recipes.Recipe", related_name="meal_plan_recipes"
    )
